#include <iostream>
#include <iomanip>
#include <vector>
#include <string>
#include <fstream>
#include <map>
using namespace std;

#include "Timer.hpp"
#include "Menu.hpp"

struct DataEntry
{
    map<string, string> fields;

    void Output( ofstream& output )
    {
        for ( map<string, string>::iterator it = fields.begin();
                it != fields.end(); it++ )
        {
            output << left << setw( 30 ) << it->second;
        }
        output << endl;
    }
};

void ReadData( vector<DataEntry>& data, const string& filename );

/* ********************************************* */
/* TODO: Add prototypes for sort functions here. */
/* ********************************************* */
void SelectionSort(vector<DataEntry> &data, string column);
void MergeSort(vector<DataEntry> &data, string column);

int main()
{
    Menu::Header( "U.S. Chronic Disease Indicators" );

    vector<string> filenames = {
        "100_US_Chronic_Disease_Indicators.csv",
        "1000_US_Chronic_Disease_Indicators.csv",
        "10000_US_Chronic_Disease_Indicators.csv",
        "523487_US_Chronic_Disease_Indicators.csv"
    };

    /* ********************************************* */
    /* TODO: Update this menu with your sorting algs */
    /* ********************************************* */
    vector<string> sorts = {
        "Selection Sort",
        "Merge Sort"
    };

    vector<string> columns = {
        "YearStart", "YearEnd", "LocationAbbr", "LocationDesc", "Topic", "Question"
    };

    cout << "Which file do you want to load?" << endl;
    int fileChoice = Menu::ShowIntMenuWithPrompt( filenames );

    cout << "Which sort do you want to use?" << endl;
    int sortChoice = Menu::ShowIntMenuWithPrompt( sorts );

    cout << "Which column do you want to sort on?" << endl;
    int sortOnChoice = Menu::ShowIntMenuWithPrompt( columns );

    Timer timer;
    vector<DataEntry> data;

    string filename = filenames[ fileChoice - 1 ];


    // Read in the data from the file
    cout << left << setw( 15 ) << "BEGIN:" << "Loading data from file, \"" << filename << "\"..." << endl;
    timer.Start();
    ReadData( data, filename );
    cout << left << setw( 15 ) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;

    cout << data.size() << " items loaded" << endl;


    // Sort the data
    if ( sortChoice == 1 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Selection Sort..." << endl;
        timer.Start();

        /* *************************************** */
        /* TODO: Call basic sorting algorithm here */
        /* *************************************** */
        SelectionSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;
    }
    else if ( sortChoice == 2 )
    {
        cout << left << setw( 15 ) << "BEGIN:" << "Sorting data with Merge Sort..." << endl;
        timer.Start();
        /* **************************************** */
        /* TODO: Call faster sorting algorithm here */
        /* **************************************** */
        MergeSort( data, columns[ sortOnChoice - 1 ] );
        cout << left << setw( 15 ) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds" << endl << endl;
    }
    cout << endl << "Writing list out to \"output.txt\"..." << endl;


    // Output the sorted data
    ofstream output( "output.txt" );
    for ( map<string, string>::iterator it = data[0].fields.begin();
            it != data[0].fields.end(); it++ )
    {
        output << left << setw( 30 ) << it->first;
    }

    output << endl;

    for ( unsigned int i = 0; i < data.size(); i++ )
    {
        data[i].Output( output );
    }
    output.close();
	system("pause");
    return 0;
}

void ReadData(vector<DataEntry>& data, const string& filename)
{
	ifstream input(filename);

	vector<string> headerItems;

	int field = 0;

	string line;
	bool header = true;
	bool skippedGeoComma = false;
	while (getline(input, line))
	{
		DataEntry entry;

		int columnBegin = 0;
		field = 0;
		skippedGeoComma = false;

		for (unsigned int i = 0; i < line.size(); i++)
		{
			//cout << line[i];
			if (line[i] == ',')
			{
				int length = i - columnBegin;
				string substring = line.substr(columnBegin, length);

				if (header)
				{
					headerItems.push_back(substring);
				}
				else
				{
					string fieldKey = "";
					if (field >= headerItems.size())
					{
						fieldKey = "Unknown";
					}
					else
					{
						fieldKey = headerItems[field];
					}
					entry.fields[fieldKey] = substring;
				}

				columnBegin = i + 1;

				if (header == false && skippedGeoComma == false && headerItems[field] == "GeoLocation")
				{
					skippedGeoComma = true;
					// Ignore this comma.
					continue;
				}
				else
				{
					field++;
				}
			}
		}

		if (header)
		{
			header = false;
		}
		else
		{
			data.push_back(entry);
		}
	}

	input.close();
}

/* ******************************************** */
/* TODO: Implement sorting algorithsm down here */
/* ******************************************** */
void swap(vector<DataEntry> &data, int index1, int index2) {
	DataEntry temp = data[index1];
	data[index1] = data[index2];
	data[index2] = temp;
}
void SelectionSort(vector<DataEntry> &data, string column) {
	for (int i = 1; i < data.size(); i++) {
		string highest = "";
		int highestIndex = 0;
		for (int j = 0; j < data.size() - 1 - i; i++) {
			if (data[j].fields[column] > highest) {
				highestIndex = j;
				highest = data[j].fields[column];
			}
		}
		swap(data, highestIndex, data.size() - 1 - i);
	}
}

void MergeSort(vector<DataEntry> &data, string column) {
	
}

vector<DataEntry> MergeSortRec(vector<DataEntry> &data, string column) {
	//base case
	if (data.size() == 1) {
		return data;
	}
	int splitIndex = (data.size() - 1) / 2;
	vector<DataEntry> dataSplit1;
	vector<DataEntry> dataSplit2;
	for (int i = 0; i <= splitIndex; i++) {
		dataSplit1[i] = data[i];
	}
	MergeSortRec(dataSplit1, column);
	for (int i = splitIndex + 1; i < data.size(); i++) {
		dataSplit2[i - splitIndex] = data[i];
	}
	MergeSortRec(dataSplit2, column);
	int loc1 = 0;
	int loc2 = 0;
	for (int i = 0; i < data.size(); i++) {
		if (dataSplit1[loc1].fields[column] > dataSplit2[loc2].fields[column]) {
			data[i] = dataSplit1[loc1];
			loc1++;
		}
		else {
			data[i] = dataSplit2[loc2];
			loc2++;
		}
	}
}