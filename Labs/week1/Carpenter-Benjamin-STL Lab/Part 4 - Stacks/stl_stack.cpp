// Lab - Standard Template Library - Part 4 - Stacks
// Benjamin, Carpenter

#include <iostream>
#include <string>
#include <stack>
using namespace std;

int main()
{
	stack<string> letters;
	bool finished = false;
	int choice;
	string input;
	cout << "Enter the next letter of the word, or UNDO to undo, or" <<
		" \n\tDONE to stop.\n";
	//Process input.
	while (!finished) {
		cin >> input;
		if (input == "UNDO") {
			cout << "\tRemoved " << letters.top() << "\n";
			letters.pop();
		}
		else if (input == "DONE") {
			finished = true;
		}
		else {
			letters.push(input);
		}
	}
	//Display finished word.
	cout << "\n\nFinished word: ";
	while (!letters.empty()) {
		cout << letters.top();
		letters.pop();
	}
    cin.ignore();
    cin.get();
    return 0;
}
