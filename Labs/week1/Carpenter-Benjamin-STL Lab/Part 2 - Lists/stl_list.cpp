// Lab - Standard Template Library - Part 2 - Lists
// Benjamin, Carpenter

#include <iostream>
#include <list>
#include <string>
using namespace std;

//Displays the list.
void DisplayList(list<string> & states) {
	for (list<string>::iterator it = states.begin();
		it != states.end(); it++) {
		cout << *it << "\t";
	}
	cout << "\n";
}

//Displays the list in reverse.
void DisplayReversedList(list<string> & states) {
	states.reverse();
	DisplayList(states);
}

//Displays the list in sorted order.
void DisplaySortedList(list<string> & states) {
	states.sort();
	DisplayList(states);
}

//Displays the list in reversed sorted order.
void DisplayReversedSortedList(list<string> & states) {
	states.sort();
	DisplayReversedList(states);
}
int main()
{
	list<string> states;
	bool finished = false;
	string input;
	while (!finished) {
		//Display list size.
		cout << "number of states : " << states.size() << "\n";

		//Display menu.
		cout << "1. Add new state to front\n2. Add new state to back\n" <<
			"3. Pop front state\n4. Pop back state\n5. Continue\n";
		int choice;
		cin >> choice;
		//Perform action.
		if (choice == 1) {
			cout << "State to add to front: ";
			cin >> input;
			states.push_front(input);
		}
		if (choice == 2) {
			cout << "State to add to back: ";
			cin >> input;
			states.push_back(input);
		}
		if (choice == 3) {
			cout << states.front() << " removed\n";
			states.pop_front();
		}
		if (choice == 4) {
			cout << states.back() <<" removed\n";
			states.pop_back();
		}
		if (choice == 5) {
			finished = true;
		}
	}
	//Display list.
	cout << "List: \n";
	DisplayList(states);
	cout << "Reversed list: \n";
	DisplayReversedList(states);
	cout << "Sorted list: \n";
	DisplaySortedList(states);
	cout << "Reversed sorted list: \n";
	DisplayReversedSortedList(states);

    cin.ignore();
    cin.get();
    return 0;
}
