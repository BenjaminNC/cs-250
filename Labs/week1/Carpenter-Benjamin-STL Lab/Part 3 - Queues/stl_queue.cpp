// Lab - Standard Template Library - Part 3 - Queues
// Benjamin, Carpenter

#include <iostream>
#include <string>
#include <queue>
using namespace std;

int main()
{
	queue<float> transactions;
	float input;
	int choice;
	bool finished = false;
	while (!finished) {
		//Display number of transactions.
		cout << "Transactions queued: " << transactions.size() << "\n";
		
		//Display Menu.
		cout << "1. Enqueue transaction\n2. Continue\n";
		
		cin >> choice;
		//Complete action.
		if (choice == 1) {
			cout << "Enter amount (positive or negative) for next"
				<< " transaction\n\t: $";
			cin >> input;
			transactions.push(input);
		}
		if (choice == 2) {
			finished = true;
		}
	}
	//Process transactions.
	float balance = 0;
	while (!transactions.empty()) {
		cout << transactions.front() << " pushed to account\n";
		balance += transactions.front();
		transactions.pop();
	}
	//Display final balance.
	cout << "\nFinal balance: $" << balance;

    cin.ignore();
    cin.get();
    return 0;
}
