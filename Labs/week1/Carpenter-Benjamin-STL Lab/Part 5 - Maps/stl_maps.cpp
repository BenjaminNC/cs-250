// Lab - Standard Template Library - Part 5 - Maps
// Benjamin, Carpenter

#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
	//Create map.
	map<char, string> colors;
	colors['r'] = "FF0000";
	colors['g'] = "00FF00";
	colors['b'] = "0000FF";
	colors['c'] = "00FFFF";
	colors['m'] = "FF00FF";
	colors['y'] = "FFFF00";
	//Process input.
	bool finished = false;
	char input;
	while(!finished){
		cout << "Enter a color letter, hex, or 'q' to stop: ";
		cin >> input;
		if (input == 'q') {
			finished = true;
		}else {
			cout << "Hex: " << colors[input] << "\n\n";
		}
	}
	cout << "Goodbye";
    cin.ignore();
    cin.get();
    return 0;
}
