// Lab - Standard Template Library - Part 1 - Vectors
// Benjamin, Carpenter

#include <iostream>
#include <vector>
#include <string>
using namespace std;

int main()
{
	vector<string> courses;

	bool done = false;
	while (!done) {
		cout << "1. Add a new course,\n"
			<< "2. Remove the last course,\n"
			<< "3. Display the course list,\n"
			<< "4. Quit" << "\n";
		int choice;
		cin >> choice;
		if(choice == 1) {
			string courseName;
			cout << "Please enter te course : ";
			cin >> courseName;
			courses.push_back(courseName);
		}
		if (choice == 2) {
			courses.pop_back();
		}
		if (choice == 3) {
			for (unsigned int i = 0; i < courses.size(); i++) {
				cout << i << ". " << courses[i] << "\n";
			}
		}
		if (choice == 4) {
			done = true;
		}
	}
    cin.ignore();
    cin.get();
    return 0;
}
