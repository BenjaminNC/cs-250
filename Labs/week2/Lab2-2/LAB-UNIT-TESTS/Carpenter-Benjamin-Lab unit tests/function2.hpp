#ifndef _function2
#define _function2

int SumArray( int arr[], int size )
{
	int sum = 0;
    for ( int i = 0; i < size; i++ )
    {
        sum += arr[i];
    }
	return sum;
}

/* Add a test to this function */
void Test_SumArr()
{
    cout << "************ Test_SumArr ************" << endl;

    int input1, input2, input3;
    int expectedOutput;
    int actualOutput;

    /* TEST 1 ********************************************/
    int inputArray1[] = { 1, 2, 3, 4 };
    expectedOutput = 10;

    actualOutput = SumArray( inputArray1, 4 );
    if ( actualOutput == expectedOutput )
    {
        cout << "Test_SumArr: Test 1 passed!" << endl;
    }
    else
    {
        cout << "Test_SumArr: Test 1 FAILED! \n\t"
        << "Inputs: 1, 2, 3, 4 \n\t"
        << "Expected: " << expectedOutput << "\n\t"
        << "Actual: " << actualOutput << endl << endl;
    }

	//!!!!!Note: tests were created before starter files were updated!!!!!
    /* TEST 2 ********************************************/
    // CREATE YOUR OWN TEST
	inputArray1[0] = 0;
	inputArray1[1] = -8;
	inputArray1[2] = 2;
	inputArray1[3] = 6;
	expectedOutput = 0;

	actualOutput = SumArray(inputArray1, 4);
	if (actualOutput == expectedOutput)
	{
		cout << "Test_SumArr: Test 2 passed!" << endl;
	}
	else
	{
		cout << "Test_SumArr: Test 2 FAILED! \n\t"
			<< "Inputs: 0, -8, 2, 6 \n\t"
			<< "Expected: " << expectedOutput << "\n\t"
			<< "Actual: " << actualOutput << endl << endl;
	}

    /* TEST 3 ********************************************/
    // CREATE YOUR OWN TEST
	inputArray1[0] = -12;
	inputArray1[1] = -8;
	inputArray1[2] = 16;
	inputArray1[3] = -50;
	expectedOutput = -54;

	actualOutput = SumArray(inputArray1, 4);
	if (actualOutput == expectedOutput)
	{
		cout << "Test_SumArr: Test 3 passed!" << endl;
	}
	else
	{
		cout << "Test_SumArr: Test 4 FAILED! \n\t"
			<< "Inputs: -12, -8, 16, -50 \n\t"
			<< "Expected: " << expectedOutput << "\n\t"
			<< "Actual: " << actualOutput << endl << endl;
	}
}

#endif

