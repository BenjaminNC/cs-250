#ifndef _PROCESSOR
#define _PROCESSOR

#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>
using namespace std;

#include "Job.hpp"
#include "Queue.hpp"

class Processor
{
    public:
    void FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile );
    void RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile );
};

void Processor::FirstComeFirstServe( vector<Job>& allJobs, Queue<Job*>& jobQueue, const string& logFile )
{
	ofstream output(logFile);
	output << "Begin FCFS\n";
	output << "Number of jobs: " << jobQueue.Size() << "\n";
	int cycle = 0;
	while (jobQueue.Size() > 0) {
		jobQueue.Front()->Work(FCFS);
		cycle++;
		output << "Cycle: " << cycle << " Job id: " << jobQueue.Front()->id << " Work remaining: " << jobQueue.Front()->fcfs_timeRemaining << "\n";
		if (jobQueue.Front()->fcfs_done) {
			output << "Job finished\n";
			jobQueue.Front()->SetFinishTime(cycle, FCFS);
			jobQueue.Pop();
		}
	}
	output << "All jobs finished\n";
	output << "Job id\tfinished in\n";
	for (int i = 0; i < allJobs.size(); i++) {
		output << allJobs[i].id << "\t" << allJobs[i].fcfs_finishTime << "\n";
	}
	output << "\n Total time: " << cycle << "\n";
	output << "Average time per process: " << cycle / allJobs.size() << "\n";
	output.close();
}

void Processor::RoundRobin( vector<Job>& allJobs, Queue<Job*>& jobQueue, int timePerProcess, const string& logFile )
{
	ofstream output(logFile);
	output << "Begin RR\n";
	output << "Number of jobs: " << jobQueue.Size() << "\n";
	int cycle = 0;
	while (jobQueue.Size() > 0) {
		for (int i = 0; i < timePerProcess; i++) {
			jobQueue.Front()->Work(RR);
			cycle++;
			output << "Cycle: " << cycle << " Job id: " << jobQueue.Front()->id << " Work remaining: " << jobQueue.Front()->rr_timeRemaining << " Times interrupted: " << jobQueue.Front()->rr_timesInterrupted << "\n";
			if (jobQueue.Front()->rr_done) {
				jobQueue.Front()->SetFinishTime(cycle, RR);
				output << "Job Finished\n";
				break;
			}
		}
		if (jobQueue.Front()->rr_done) {
			jobQueue.Pop();
		}
		else {
			jobQueue.Front()->rr_timesInterrupted++;
			jobQueue.Push(jobQueue.Front());
			jobQueue.Pop();
		}
	}
	output << "All jobs finished\n";
	output << "Job id\tfinished in\n";
	for (int i = 0; i < allJobs.size(); i++) {
		output << allJobs[i].id << "\t" << allJobs[i].rr_finishTime << " Times interrupted: " << allJobs[i].rr_timesInterrupted << "\n";
	}
	output << "\n Total time: " << cycle << "\n";
	output << "Average time per process: " << cycle / allJobs.size() << "\n";
	output << "Interval: " << timePerProcess << "\n";
	output.close();
}

#endif
