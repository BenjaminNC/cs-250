#ifndef _LIST_HPP
#define _LIST_HPP

template <typename T>
class List
{
private:
    // private member variables
    int m_itemCount;
    const static int ARRAY_SIZE = 100;
	T array[ARRAY_SIZE];
    // functions for interal-workings
    bool ShiftRight( int atIndex )
    {
		if (IsFull() || IsEmpty() || atIndex >= m_itemCount || atIndex < 0) {
			return false;
		}
		for (int i = m_itemCount - 1; i >= atIndex; i--) {
			array[i + 1] = array[i];
		}
        return true;
    }

    bool ShiftLeft( int atIndex )
    {
		if (IsEmpty() || atIndex >= m_itemCount || atIndex < 0) {
			return false;
		}
		for (int i = atIndex; i < m_itemCount; i++) {
			array[i] = array[i + 1];
		}
		return true;
    }

public:
    List()
    {
		m_itemCount = 0;
    }

    ~List()
    {
    }

    // Core functionality
    int     Size() const
    {
		return m_itemCount;
    }

    bool    IsEmpty() const
    {
		return (m_itemCount == 0);
    }

    bool    IsFull() const
    {
        return (m_itemCount == ARRAY_SIZE) ? true : false;
    }

    bool    PushFront( const T& newItem )
    {
		if (ShiftRight(0) || IsEmpty()) {
			array[0] = newItem;
			m_itemCount++;
			return true;
		}
		else {
			return false;
		}
    }

    bool    PushBack( const T& newItem )
    {
		if (!IsFull()) {
			array[m_itemCount] = newItem;
			m_itemCount++;
			return true;
		}
		else {
			return false;
		}
    }

    bool    Insert( int atIndex, const T& item )
    {
		if (atIndex < 0 || atIndex > m_itemCount || IsFull()) {
			return false;
		}
		//Uses shortcut so that ShiftLeft does not run if array is empty.
		else if(!IsEmpty() && !ShiftRight(atIndex)) {
			return false;
		}
		else {
			array[atIndex] = item;
			m_itemCount++;
			return true;
		}
    }

    bool    PopFront()
    {
		if (IsEmpty()) {
			return false;
		}
		else if(!ShiftLeft(0)) {
			return false;
		}
		else {
			m_itemCount--;
			return true;
		}
    }

    bool    PopBack()
    {
		if (IsEmpty()) {
			return false;
		}
		else {
			m_itemCount--;
			return true;
		}
    }

    bool    Remove( const T& item )
    {
		if (IsEmpty()) {
			return false;
		}
		else {
			for (int i = 0; i < m_itemCount; i++) {
				//Uses shortcut so that ShiftLeft does not run if array[i] == item is false.
				if (array[i] == item && ShiftLeft(i)) {
					m_itemCount--;
					return true;
				}
			}
			return false;
		}
    }

    bool    RemoveIndex( int atIndex )
    {
		if (IsEmpty() || atIndex < 0 || atIndex >= m_itemCount) {
			return false;
		}
		else if(!ShiftLeft(atIndex)) {
			return false;
		}
		else {
			m_itemCount--;
			return true;
		}
    }

    void    Clear()
    {
		m_itemCount = 0;
    }

    // Accessors
    T*      Get( int atIndex )
    {
		if (IsEmpty() || atIndex < 0 || atIndex >= m_itemCount) {
			return nullptr;
		}
		else {
			return &array[atIndex];
		}
    }

    T*      GetFront()
    {
		if (IsEmpty()) {
			return nullptr;
		}
		else {
			return &array[0];
		}
       
    }

    T*      GetBack()
    {
		if (IsEmpty()) {
			return nullptr;
		}
		else {
			return &array[m_itemCount - 1];
		}
    }

    // Additional functionality
    int     GetCountOf( const T& item ) const
    {
		if (IsEmpty()) {
			return 0;
		}
		else {
			int count = 0;
			for (int i = 0; i < m_itemCount; i++) {
				if (array[i] == item) {
					count++;
				}
			}
			return count;
		}
    }

    bool    Contains( const T& item ) const
    {
		if (IsEmpty()) {
			return 0;
		}
		else {
			for (int i = 0; i < m_itemCount; i++) {
				if (array[i] == item) {
					return true;
				}
			}
			return false;
		}
    }

    friend class Tester;
};


#endif
