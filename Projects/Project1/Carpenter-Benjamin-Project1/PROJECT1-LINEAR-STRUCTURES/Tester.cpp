#include "Tester.hpp"

void Tester::RunTests()
{
    Test_IsEmpty();
    Test_IsFull();
    Test_Size();
    Test_GetCountOf();
    Test_Contains();

    Test_PushFront();
    Test_PushBack();

    Test_Get();
    Test_GetFront();
    Test_GetBack();

    Test_PopFront();
    Test_PopBack();
    Test_Clear();

    Test_ShiftRight();
    Test_ShiftLeft();

    Test_Remove();
    Test_Insert();
}

void Tester::DrawLine()
{
    cout << endl;
    for ( int i = 0; i < 80; i++ )
    {
        cout << "-";
    }
    cout << endl;
}

void Tester::Test_Init()
{
    DrawLine();
    cout << "TEST: Test_Init" << endl;
	
    // Put tests here
	cout << "Test1\n";
	List<int> list;
	int EO = 0;
	int AO = list.Size();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: none\nEO: size = " << EO << "\nAO: " << AO << "\n\n";
	}
}

void Tester::Test_ShiftRight()
{
    DrawLine();
    cout << "TEST: Test_ShiftRight" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	int in = 0;
	bool EO = false;
	bool AO = list.ShiftRight(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO 
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	list2.PushFront(1);
	in = 0;
	EO = true;
	AO = list2.ShiftRight(in);
	list2.m_itemCount++; //Correct m_itemCount to accuratly reflect number of items in array(because m_itemCount is not updated in shiftRight).
	if (EO == AO && *list2.Get(1) == 1) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << 
			" and 1 in index 1" << "\nAO: " << AO << "\n\n";
	}

	cout << "Test3\n";
	List<int> list3;
	list3.PushFront(1);
	in = -5;
	EO = false;
	AO = list3.ShiftRight(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test4\n";
	List<int> list4;
	for (int i = 0; i < 100; i++) {
		list4.PushFront(i);
	}
	in = 0;
	EO = false;
	AO = list4.ShiftRight(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test5\n";
	List<int> list5;
	list5.PushFront(1);
	in = list5.Size();
	EO = false;
	AO = list5.ShiftRight(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

}

void Tester::Test_ShiftLeft()
{
    DrawLine();
    cout << "TEST: Test_ShiftLeft" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	int in = 0;
	bool EO = false;
	bool AO = list.ShiftLeft(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	list2.PushFront(1);
	in = 0;
	EO = true;
	AO = list2.ShiftLeft(in);
	list2.m_itemCount--; //Correct m_itemCount to accuratly reflect number of items in array(because m_itemCount is not updated in shiftLeft).
	if (EO == AO && list2.Get(0) == nullptr) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO <<
			" and nullptr in index 0" << "\nAO: " << AO << " and index0:"
			<< list2.Get(1) << "\n\n";
	}

	cout << "Test3\n";
	List<int> list3;
	in = -1;
	EO = false;
	AO = list3.ShiftLeft(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test4\n";
	List<int> list4;
	for (int i = 0; i < 100; i++) {
		list4.PushFront(i);
	}
	in = 0;
	EO = true;
	AO = list4.ShiftLeft(in);
	list4.m_itemCount--; //Correct m_itemCount to accuratly reflect number of items in array(because m_itemCount is not updated in shiftLeft).
	if (EO == AO && list4.Get(99) == nullptr) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << 
			" and GetBack() = nullptr" << "\nAO: " << AO << 
			" and GetBack() = " << list4.GetBack() << "\n\n";
	}

	cout << "Test5\n";
	List<int> list5;
	list5.PushFront(1);
	in = list5.Size();
	EO = false;
	AO = list5.ShiftLeft(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_Size()
{
    DrawLine();
    cout << "TEST: Test_Size" << endl;

    {   // Test begin
        cout << endl << "Test 1" << endl;
        List<int> testList;
        int expectedSize = 0;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end

    {   // Test begin
        cout << endl << "Test 2" << endl;
        List<int> testList;

        testList.PushBack( 1 );

        int expectedSize = 1;
        int actualSize = testList.Size();

        cout << "Expected size: " << expectedSize << endl;
        cout << "Actual size:   " << actualSize << endl;

        if ( actualSize == expectedSize )
        {
            cout << "Pass" << endl;
        }
        else
        {
            cout << "Fail" << endl;
        }
    }   // Test end
}

void Tester::Test_IsEmpty()
{
    DrawLine();
    cout << "TEST: Test_IsEmpty" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	bool EO = true;
	bool AO = list.IsEmpty();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	list2.PushFront(1);
	EO = false;
	AO = list2.IsEmpty();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_IsFull()
{
    DrawLine();
    cout << "TEST: Test_IsFull" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	bool EO = false;
	bool AO = list.IsFull();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	for (int i = 0; i < 100; i++) {
		list2.PushFront(i);
	}
	EO = true;
	AO = list2.IsFull();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_PushFront()
{
    DrawLine();
    cout << "TEST: Test_PushFront" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	int in = 1;
	bool EO = true;
	bool AO = list.PushFront(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	for (int i = 0; i < 100; i++) {
		list2.PushFront(i);
	}
	in = 1;
	EO = false;
	AO = list2.PushFront(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_PushBack()
{
    DrawLine();
    cout << "TEST: Test_PushBack" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	int in = 1;
	bool EO = true;
	bool AO = list.PushBack(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	for (int i = 0; i < 100; i++) {
		list2.PushFront(i);
	}
	in = 1;
	EO = false;
	AO = list2.PushBack(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_PopFront()
{
    DrawLine();
    cout << "TEST: Test_PopFront" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	bool EO = false;
	bool AO = list.PopFront();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	list2.PushFront(1);
	EO = true;
	AO = list2.PopFront();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_PopBack()
{
    DrawLine();
    cout << "TEST: Test_PopBack" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	bool EO = false;
	bool AO = list.PopBack();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	list2.PushFront(1);
	EO = true;
	AO = list2.PopBack();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_Clear()
{
    DrawLine();
    cout << "TEST: Test_Clear" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	list.PushFront(1);
	list.PushFront(2);
	list.Clear();
	int EO = 0;
	int AO = list.Size();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_Get()
{
    DrawLine();
    cout << "TEST: Test_Get" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	int in = 0;
	int* EO = nullptr;
	int* AO = list.Get(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	in = -3;
	EO = nullptr;
	AO = list2.Get(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test3\n";
	List<int> list3;
	int item = 1; //Track address
	list3.PushFront(item);
	in = 0;
	EO = &item;
	AO = list3.Get(in);
	if (*EO == *AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test4\n";
	List<int> list4;
	item = 1; //Track address.
	list4.PushFront(item);
	in = list4.Size() + 1;
	EO = nullptr;
	AO = list4.Get(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_GetFront()
{
    DrawLine();
    cout << "TEST: Test_GetFront" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	int* EO = nullptr;
	int* AO = list.GetFront();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	int item = 1; //Track address.
	list2.PushFront(item);
	EO = &item;
	AO = list2.GetFront();
	if (*EO == *AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_GetBack()
{
    DrawLine();
    cout << "TEST: Test_GetBack" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	int* EO = nullptr;
	int* AO = list.GetBack();
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	int item = 1; //Track address.
	list2.PushFront(item);
	EO = &item;
	AO = list2.GetBack();
	if (*EO == *AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed" << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_GetCountOf()
{
    DrawLine();
    cout << "TEST: Test_GetCountOf" << endl;

    // Put tests here
	cout << "Test1\n";
	List<int> list;
	int in = 1;
	int EO = 0;
	int AO = list.GetCountOf(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	list2.PushFront(1);
	in = 1;
	EO = 1;
	AO = list2.GetCountOf(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test3\n";
	List<int> list3;
	list3.PushFront(1);
	list3.PushFront(2);
	in = 1;
	EO = 1;
	AO = list3.GetCountOf(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_Contains()
{
    DrawLine();
    cout << "TEST: Test_Contains" << endl;

    // Put tests here

	cout << "Test1\n";
	List<int> list;
	list.PushFront(1);
	int in = 1;
	bool EO = true;
	bool AO = list.Contains(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	in = 1;
	EO = false;
	AO = list2.Contains(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}
}

void Tester::Test_Remove()
{
    DrawLine();
    cout << "TEST: Test_Remove" << endl;

    // Put tests here
	cout << "Test1\n";
	List<double> list; //Type changed to remove ambiguity.
	double in = 1.5;
	bool EO = false;
	bool AO = list.Remove(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test2\n";
	List<double> list2; //Type changed to remove ambiguity.
	list2.PushFront(1.5);
	in = 1.5;
	EO = true;
	AO = list2.Remove(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test3\n";
	List<double> list3; //Type changed to remove ambiguity.
	list3.PushFront(1.5);
	in = 2.5;
	EO = false;
	AO = list3.Remove(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test4\n";
	List<double> list4; //Type changed to remove ambiguity.
	list4.PushFront(1.5);
	list4.PushFront(1.5);
	list4.PushFront(2.5);
	in = 1.5;
	EO = true;
	AO = list4.Remove(in);
	if (EO == AO && *list4.Get(0) == 2.5) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << 
			" and index2 = 2.5" << "\nAO: " << AO << "\n\n";
	}

	cout << "Test5\n";
	List<double> list5; //Type changed to remove ambiguity.
	in = 0;
	EO = false;
	AO = list5.RemoveIndex(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test6\n";
	List<double> list6; //Type changed to remove ambiguity.
	list6.PushFront(1.5);
	in = 0;
	EO = true;
	AO = list6.RemoveIndex(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test7\n";
	List<double> list7; //Type changed to remove ambiguity.
	in = -3;
	EO = false;
	AO = list7.RemoveIndex(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

	cout << "Test8\n";
	List<double> list8; //Type changed to remove ambiguity.
	list8.PushFront(1.5);
	in = list8.Size() + 1;
	EO = false;
	AO = list8.RemoveIndex(in);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in << "\nEO: " << EO << "\nAO: " << AO
			<< "\n\n";
	}

}

void Tester::Test_Insert()
{
    DrawLine();
    cout << "TEST: Test_Insert" << endl;

    // Put tests here

	cout << "Test1\n";
	List<int> list;
	int in1 = 0;
	int in2 = 1;
	bool EO = true;
	bool AO = list.Insert(in1, in2);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in1 << ", " << in2 << "\nEO: " << EO 
			<< "\nAO: " << AO << "\n\n";
	}

	cout << "Test2\n";
	List<int> list2;
	list2.PushFront(1);
	in1 = 0;
	in2 = 2;
	EO = true;
	AO = list.Insert(in1, in2);
	if (EO == AO && *list2.Get(0) == 1) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in1 << ", " << in2 << "\nEO: " << EO << 
			" and index1 = 1" << "\nAO: " << AO << "\n\n";
	}

	cout << "Test3\n";
	List<int> list3;
	in1 = -5;
	in2 = 1;
	EO = false;
	AO = list3.Insert(in1, in2);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in1 << ", " << in2 << "\nEO: " << EO
			<< "\nAO: " << AO << "\n\n";
	}

	cout << "Test4\n";
	List<int> list4;
	for (int i = 0; i < 100; i++) {
		list4.PushFront(i);
	}
	in1 = 0;
	in2 = 1;
	EO = false;
	AO = list4.Insert(in1, in2);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in1 << ", " << in2 << "\nEO: " << EO
			<< "\nAO: " << AO << "\n\n";
	}

	cout << "Test5\n";
	List<int> list5;
	list5.PushFront(1);
	in1 = list5.Size() + 1;
	in2 = 1;
	EO = false;
	AO = list5.Insert(in1, in2);
	if (EO == AO) {
		cout << "passed\n\n";
	}
	else {
		cout << "failed\ninputs: " << in1 << ", " << in2 << "\nEO: " << EO
			<< "\nAO: " << AO << "\n\n";
	}
}
