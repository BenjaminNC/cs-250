#ifndef _STACK_HPP
#define _STACK_HPP

#include "Node.hpp"

template <typename T>
class LinkedStack
{
    public:
    LinkedStack()
    {
		m_itemCount = 0;
		m_ptrFirst = nullptr;
		m_ptrLast = nullptr;
    }

    void Push( const T& newData ) noexcept
    {
		Node<T>* newNode = new Node<T>;
		newNode->data = newData;
		if (Size() == 0) {
			m_ptrFirst = newNode;
			m_ptrLast = newNode;
		}
		else {
			newNode->ptrNext = m_ptrFirst;
			newNode->ptrNext->ptrPrev = newNode;
			m_ptrFirst = newNode;
		}
    }

    T& Top()
    {
		if (Size() == 0) {
			throw runtime_error("Stack empty");
		}
		else {
			return m_ptrFirst->data;
		}
    }

    void Pop()
    {
		if (Size() > 0) {
			if (Size() == 1) {
				delete m_ptrFirst;
				m_ptrFirst = nullptr;
				m_ptrLast = nullptr;
			}
			else {
				m_ptrFirst = m_ptrFirst->ptrNext;
				delete m_ptrFirst->ptrPrev;
				m_ptrFirst->ptrPrev = nullptr;
			}
		}
    }

    int Size()
    {
		int count = 0;
		Node<T>* currentNode = m_ptrFirst;
		while (currentNode != nullptr) {
			count++;
			currentNode = currentNode->ptrNext;
		}
		return count;
    }

    private:
    Node<T>* m_ptrFirst;
    Node<T>* m_ptrLast;
    int m_itemCount;
};

#endif
