#include "CourseCatalog.hpp"

#include <iostream>
#include <iomanip>
#include <fstream>
#include "DATA_STRUCTURES\Stack.hpp"
using namespace std;

#include "UTILITIES/Menu.hpp"
#include "EXCEPTIONS/CourseNotFoundException.hpp"

CourseCatalog::CourseCatalog()
{
    LoadCourses();
}

void CourseCatalog::LoadCourses() // done
{
    Menu::Header( "LOADING COURSES" );

    ifstream input( "courses.txt" );

    if ( !input.is_open() )
    {
        cout << "Error opening input text file, courses.txt" << endl;
        return;
    }

    string label, courseCode, courseName, prerequisite;
    Course newCourse;

    while ( input >> label )
    {
        if ( label == "COURSE" )
        {
            if ( newCourse.name != "" )
            {
                m_courses.PushBack( newCourse );
                newCourse.Clear();
            }

            input >> newCourse.code >> newCourse.name;
        }
        else if ( label == "PREREQ" )
        {
            input >> newCourse.prereq;
        }
    }

    input.close();

    cout << " * " << m_courses.Size() << " courses loaded" << endl << endl;
}

void CourseCatalog::ViewCourses() noexcept
{
    Menu::Header( "VIEW COURSES" );
	cout << "\n#\tCODE\tTITLE\n\t  PREREQS\n" <<
		"---------------------------------------------\n";
	for (int i = 0; i < m_courses.Size(); i++) {
		cout << i  << "\t" << m_courses[i].code << "\t" << m_courses[i].name 
			<< "\n";
		if (m_courses[i].prereq != "") {
			cout << "\t  " << m_courses[i].prereq << "\n";
		}
		cout << "\n";
	}
}

Course CourseCatalog::FindCourse( const string& code )
{
	if (m_courses.IsEmpty()) {
		throw CourseNotFound("Course list empty");
	}
	else {
		for (int i = 0; i < m_courses.Size(); i++) {
			if (m_courses[i].code == code) {
				return m_courses[i];
			}
		}
		throw CourseNotFound("Not found");
	}
}

void CourseCatalog::ViewPrereqs() noexcept
{
    Menu::Header( "GET PREREQS" );
	string input = "";
	Course current;
	cout << "\nEnter class code\n\n>> ";
	cin >> input;
	cout << "\n";
	try {
		current = FindCourse(input);
	}
	catch (CourseNotFound e) {
		cout << "Error! Unable to find course " << input << "!\n";
		return;
	}
	
	LinkedStack<Course> prereqStack;
	prereqStack.Push(current);
	while (current.prereq != "") {
		try {
			current = FindCourse(current.prereq);
		}
		catch (CourseNotFound e) {
			break;
		}
		prereqStack.Push(current);
	}
	//print prereq stack
	cout << "Classes to take:\n";
	int number = 1;
	while (prereqStack.Size() > 0) {
		cout << number << ".\t" << prereqStack.Top().code << " " << prereqStack.Top().name << "\n";
		prereqStack.Pop();
		number++;
	}
}

void CourseCatalog::Run()
{
    bool done = false;
    while ( !done )
    {
        Menu::Header( "MAIN MENU" );

        int choice = Menu::ShowIntMenuWithPrompt( { "View all courses", "Get course prerequisites", "Exit" } );

        switch( choice )
        {
            case 1:
                ViewCourses();
            break;

            case 2:
                ViewPrereqs();
            break;

            case 3:
                done = true;
            break;
        }
    }
}
