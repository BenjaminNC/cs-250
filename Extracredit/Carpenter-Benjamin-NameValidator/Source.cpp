/**
 * @author Benjamin Carpenter
 * Extra credit Name Validator
*/
#include <iostream>
#include <fstream>
#include <string>
#include<vector>
using namespace std;
//Characters that should not be in a name.
const vector<char> INVALID_CHARS = { '1', '2', '3', '4', '5', 
	'6', '7', '8', '9', '!', '-' };

/**
 * validateChar checks to see if a given character is valid.
 * @param invalidChars a vector that contains all the invalid characters.
 * @param c the char to validate.
 * @return true if the character is valid or false otherwise.
*/
bool validateChar(vector<char> invalidChars, char c) {
	for (int i = 0; i < invalidChars.size(); i++) {
		if (invalidChars[i] == c) {
			return false;
		}
	}
	return true;
}

/**
 * validateName validates a given name to make ensure it doesn't contain any
 * invalid characters.
 * @param name the name to validate.
 * @return true if the name is valid or false otherwise.
*/
bool validateName(string name) {
	for (int i = 0; i < name.length(); i++) {
		if (!validateChar(INVALID_CHARS, name[i])) {
			return false;
		}
	}
	return true;
}
void main(){
	vector<string> names; //The pool of current names.
	bool finished = false;
	int choice;

	//Create file i/o objects.
	ifstream fileIn;
	ofstream fileOut;
	fileOut.open("names.txt", ios::app);//Ensure file exists.
	fileIn.open("names.txt");

	//Initialize names.
	string inputName;
	while (!fileIn.eof()) {
		getline(fileIn, inputName);
		names.push_back(inputName);
	}
	fileIn.close();

	//Display names if any exist.
	if (names.size() > 1) {
		cout << "Names on file:\n";
		for (int i = 0; i < names.size() - 1; i++) {
			cout << i + 1 << ". " << names[i] << "\n";
		}
	}
	else {
		cout << "No names on file.";
	}
	cout << "\n\n";
	while (!finished) {
		
		//Display menu.
		cout << "1. Enter a new name\n2. Quit\n:";

		//Get and process input.
		cin >> choice;
		cout << "\n";
		if (choice == 1) {
			bool valid = false;
			string name;
			
			//Get and validate name until name is in a valid state.
			while (!valid) {
				cout << "Enter a full name without !,-, or numbers\n:";
				cin.ignore();
				getline(cin, name);
				if (validateName(name)) {
					valid = true;
				}
				else {
					cout << "Invalid name.\n\n";
				}
			}
			
			//Append name to file.
			fileOut << name << "\n";
		}
		if(choice == 2){
			finished = true;
		}
	}
	fileOut.close();
	system("pause");
}